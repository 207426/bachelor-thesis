# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# Vagrant file for Ubuntu 12.04 LTS with Argos honeypot
#
#
# Author: Tomas Plesnik       <plesnik@ics.muni.cz>
#

# Import yaml module for parsing of the configuration file
begin
    require 'yaml'
rescue
    `gem install yaml`
end

# Load yaml configuration file
conf = YAML::load_file('vagrant.conf')

# Check out presence of Windows 7 QCOW image
if !File.file?('images/' + conf["image"].to_s)
    STDERR.puts "Could not find Windows 7 QCOW image!"
    exit(false)
end

Vagrant.configure("2") do |config|

  # A Vagrant plugin to keep your VirtualBox Guest Additions up to date
  unless Vagrant.has_plugin?("vagrant-vbguest")
    system "vagrant plugin install vagrant-vbguest"
  end

  # Set networking of the box
  config.vm.network "private_network", type: "dhcp"

  # Name of Vagrant box - image for virtual machine
  config.vm.box = "hashicorp/precise64"
  config.vm.box_check_update = false
  config.vm.hostname = conf["hostname"]

  # Base Box uses rsync for default sharing, has to be explicitly disabled for Windows hosts
  config.vm.synced_folder '.', '/vagrant', type: "rsync",
  rsync__exclude: Dir.entries('.').select { |entry| entry if not conf["shared_directories"].include?(entry) }

  # Set HW for virtual machine
  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--name", conf['vm_name']]
    vb.customize ["modifyvm", :id, "--memory", conf['memory']]
    vb.customize ["modifyvm", :id, "--cpus", conf['cpus']]
    vb.customize ["modifyvm", :id, "--vram", conf['vram']]
  end

  # Prepare virtual machine environment (install ansible and run ansible playbook)
  config.vm.provision "ansible_local" do |ansible|
    ansible.install = true
    ansible.become = true
    ansible.verbose = "v"
    ansible.playbook = 'ansible/playbook.yml'
  end

end
