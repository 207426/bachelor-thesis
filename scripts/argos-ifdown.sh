#!/bin/sh

brctl delif br0 eth0
brctl delif br0 tap0
ip a flush dev br0
ifconfig br0 down
brctl delbr br0
ip a flush dev tap0
tunctl -d tap0
dhclient eth0

exit 0
