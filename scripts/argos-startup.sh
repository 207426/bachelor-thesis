#!/bin/bash

#/usr/local/bin/argos-system-i386 -m 4G -snapshot -hda /root/argos/images/windows7.img -net nic,model=rtl8139 -net tap,ifname=tap0,script=/root/argos/scripts/argos-ifup.sh,downscript=/root/argos/scripts/argos-ifdown.sh

/usr/local/bin/argos-system-i386 -m 4096M -snapshot -hda /root/argos/images/windows7-pro-32bit-cz.img -net nic,model=e1000 -net tap,ifname=tap0,script=/root/argos/scripts/argos-ifup.sh,downscript=/root/argos/scripts/argos-ifdown.sh

exit 0


# Testing:
#
# /usr/local/bin/argos-system-i386 -m 4096M -sdl -enable-kvm -snapshot -smp cpus=1,cores=2 -hda /root/argos/images/windows7-pro-32bit-cz.img -net nic,model=e1000 -net tap,ifname=tap0,script=/root/argos/scripts/argos-ifup.sh,downscript=/root/argos/scripts/argos-ifdown.sh