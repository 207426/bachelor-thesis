#!/bin/sh

echo 1 > /proc/sys/net/ipv4/ip_forward

brctl addbr br0
brctl addif br0 eth0
tunctl -t tap0 -u root
brctl addif br0 tap0
ifconfig eth0 promisc up
ifconfig tap0 0.0.0.0 promisc up
ifconfig br0 192.168.1.80 up
route add default gw 192.168.1.1

exit 0
