# Využití honeypotů pro zkoumání exploitů (Using honeypots for exploit analysis)

## Systémové požadavky

* Instalace nástroje [VirtualBox](https://www.virtualbox.org/wiki/Downloads).
* Instalace nástroje [Vagrant](https://www.vagrantup.com/downloads.html).

## Automatizované sestavení honeypotu Argos

* Otevřít terminál.
* Stáhnout repozitář s projektem:
 
```
$ git clone git@gitlab.ics.muni.cz:plesnik/bachelor-thesis.git
```

* Přesunout se do adresáře projektu:

```
$ cd bachelor-thesis
```

* Nastavit parametry VM stroje:

```
$ vim vagrant.conf
```

* Nahrát Windows 7 QCOW image do adresáře ```images```:

```
$ cp windows7.img images
```

* Spustit VM:

```
$ vagrant up
```

## Spuštění honeypotu Argos

* Otevření okna VM ```argos-honeypot``` ve VirtualBoxu

* Přepnout se na roota:

```
vagrant@argos-0-7-0 $ sudo su -
```

* Přesunout se do adresáře ```/root/argos```:

```
vagrant@argos-0-7-0 # cd argos
```

* Spustit honeypot pomocí startovacího skriptu ```argos-startup.sh```:

```
vagrant@argos-0-7-0 # ./scripts/argos-startup.sh
```